<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Activity 1</title>
</head>
<body>
	<h1>Activity 1</h1>

    <h1>Full Address</h1>
    <p> <?php echo getFullAddress("Ramfaya Village", "San Fernando City", "La Union", "Philippines"); ?> </p>

    <h1>Activity 2</h1>

    <h1>Letter Grade</h1>
    <p>60 is equivalent to <?php echo getLetterGrade(60); ?></p>
    <p>75 is equivalent to <?php echo getLetterGrade(75); ?></p>
    <p>77 is equivalent to <?php echo getLetterGrade(77); ?></p>
    <p>80 is equivalent to <?php echo getLetterGrade(80); ?></p>
    <p>83 is equivalent to <?php echo getLetterGrade(83); ?></p>
    <p>86 is equivalent to <?php echo getLetterGrade(86); ?></p>
    <p>89 is equivalent to <?php echo getLetterGrade(89); ?></p>
    <p>92 is equivalent to <?php echo getLetterGrade(92); ?></p>
    <p>95 is equivalent to <?php echo getLetterGrade(95); ?></p>
    <p>98 is equivalent to <?php echo getLetterGrade(98); ?></p>

  
</body>
</html>
